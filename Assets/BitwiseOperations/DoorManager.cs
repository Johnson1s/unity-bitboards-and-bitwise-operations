using System;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    private int doorType = AttributeManager.Magic | AttributeManager.Charisma;

    private void OnCollisionEnter(Collision collision)
    {
        var attributeManager = collision.gameObject.GetComponent<AttributeManager>();
        if ((attributeManager.attributes & doorType) == doorType)
        {
            GetComponent<Collider>().isTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GetComponent<Collider>().isTrigger = false;
        
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}